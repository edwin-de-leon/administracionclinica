from django.contrib import messages
from django.views.generic.base import ContextMixin
from django.views.generic.edit import ModelFormMixin


class CreateMessageMixin(ModelFormMixin):
    def form_valid(self, form):
        item = form.save()
        messages.success(self.request, 'Se ha creado el exitosamente el registro "%s".' % item)
        return super().form_valid(form)


class UpdateMessageMixin(ModelFormMixin):
    def form_valid(self, form):
        item = form.save()
        messages.warning(self.request, 'Se ha editado exitosamente el registro "%s".' % item)
        return super().form_valid(form)


class CancelUrlMixin(ContextMixin):
    cancel_url = '#'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cancel_url'] = self.cancel_url
        return context


class CreateFormResponseMixin(ModelFormMixin):
    def form_valid(self, form):
        """ Guardamos y volvemos a mostrar el formulario vacio """
        form.save()
        form_class = self.get_form_class()
        return self.render_to_response(self.get_context_data(form=form_class()))


class UpdateFormResponseMixin(ModelFormMixin):
    def form_valid(self, form):
        """ Guardamos y volvemos a mostrar el formulario vacio """
        self.object = form.save()
        return self.render_to_response(self.get_context_data())
