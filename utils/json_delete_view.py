from django.http import JsonResponse
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import DeletionMixin


class JsonDeleteView(SingleObjectMixin, DeletionMixin, View):
    """
    Permite borrar una instrancia de un objeto devolviendo unicamente un JSON como respuesta
    """
    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        self.object.delete()
        return JsonResponse({'result': 'OK'})
