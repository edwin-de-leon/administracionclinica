from django.core import serializers
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.views.generic.base import View


class DataTableListView(View):
    """
    Clase estandar para paginacion, busqueda y ordenacion con Datatables
    """
    queryset = None
    sort_direction = None
    search = ''
    per_page = None
    sort_field = None
    page = None
    draw = None
    total = None

    def get_request_data(self, request):
        request_sort_direction = request.POST.get('sort[sort]', 'asc')
        request_page = request.POST.get('pagination[page]', 1)
        self.sort_direction = '' if request_sort_direction == 'asc' else '-'
        self.search = request.POST.get('query[generalSearch]', '')
        self.per_page = request.POST.get('pagination[perpage]', '10')
        self.sort_field = request.POST.get('sort[field]', 'nombre')
        self.page = 1 if request_page == '' else int(request_page)
        self.draw = request.POST.get('draw', 1)

    def filter_queryset(self, queryset, search):
        """
        Manejar aca los filtros para la consulta
        :param search: parametro de busqueda
        :param queryset: lista a consultar
        :return:
        """
        return queryset

    def paginate_queryset(self):
        """
        Pagina el queryset de la clase
        :return: diccionario para serializar con json, paginador
        """
        # obtenemos los productos y aplicamos inmediatamente el filtro
        queryset = self.queryset
        self.total = self.queryset.count()
        if self.search is not '':
            queryset = self.filter_queryset(self.queryset, self.search)

        # Ordenamos la busqueda segun los datos que nos han indicado
        queryset = queryset.order_by('%s%s' % (self.sort_direction, self.sort_field))

        paginator = Paginator(queryset, self.per_page)
        try:
            queryset = paginator.page(self.page)
        except PageNotAnInteger:
            queryset = paginator.page(1)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)

        # Creamos un diccionario con los datos que necesitamos
        return list(ob.as_json() for ob in queryset), paginator

    def post(self, request):
        self.get_request_data(request)
        results, paginator = self.paginate_queryset()

        return JsonResponse(
            {
                'result': 'OK',
                'data': results,
                'draw': self.draw,
                'recordsTotal': self.total,
                'recordsFiltered': paginator.count
            }
        )
