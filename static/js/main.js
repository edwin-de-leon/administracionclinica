$(document).ready(function(){
    closeAlert();
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function closeAlert() {
    window.setTimeout(function() {
        $("div.alert").fadeTo(1000, 0.8).slideUp(1000, function(){
            $(this).remove();
        });
    }, 5000);
}

function add_class_checkbox_label(id_content) {
    var target = document.getElementById(id_content).getElementsByTagName("label")[0];
    target.className += " m-checkbox m-checkbox--air m-checkbox--state-success";
    target.insertAdjacentHTML('beforeend', "<span></span>");
}

$.fn.serializeFormJSON = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function loadComponents(parent){
    try {
        $('input.dateinput').datetimepicker({
            format: "L",
            locale: "es",
            useStrict: true,
            widgetParent: parent,
            keyBinds: {
                enter: function () {
                    this.hide();
                },
                escape: function () {
                    this.hide();
                }
            }
        });
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
            throw e;
        }
    }

    try {
        $('input[type="checkbox"], input[type="radio"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
            throw e;
        }
    }

    try {
        $('input.currency').inputmask("numeric", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            prefix: 'Q ',
            rightAlign: false,
            min: 0,
            oncleared: function () { self.Value(''); }
        });
        $('input.number').inputmask("numeric", {
            groupSeparator: ",",
            digits: 0,
            autoGroup: true,
            rightAlign: false,
            min: 0,
        });

        $('input.percent').inputmask("numeric", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            suffix: ' %',
            rightAlign: false,
            min: 0,
            max: 99.99,
            oncleared: function () { self.Value(''); }
        });
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
            throw e;
        }
    }

    try {
        $('select.select2').select2({
            theme: 'bootstrap4',
            dropdownParent: parent,
            width: '100%'
        });
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
            throw e;
        }
    }

    $('input:visible:enabled:first', parent).focus();
}

function loadDefaultComponents() {
    loadComponents(null);
}

function loadModalDefaultComponents(parent) {
    loadComponents(parent);
}