var $defaultTable = $('#data-table');
var typingTimer; //timer identifier
var doneTypingInterval = 750;

function doneTyping () {
    $defaultTable.DataTable().search(search.value).draw();
}

function getDefaultDataTables(url, language_url, columns) {
    return  {
        ajax: {
            url: url,
            type: 'post',
            headers: { 'X-CSRFToken': getCookie('csrftoken')},
        },
        language: {
            url: language_url
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        searching: true,
        sDom: "<tr><'row'<'col-md-12 col-lg-4'l><'col-md-12 col-lg-8 text-right'i><'col-md-12'p>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columns: columns
    };
}

function deleteDataTableItem(url, name, datatable) {
    swal({
        title: '¿Desea eliminar el registro "' + name + '"?',
        text: "¡Esto no se puede revertir!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '¡Si, Eliminar!',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#d33',
        confirmButtonClass: 'btn btn-danger'
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                url: url,
                method: 'POST',
                headers: { 'X-CSRFToken': getCookie('csrftoken')},
                success: function(data) {
                    if(data.result === 'OK') {
                        swal("¡Eliminado!", "Se ha eliminado el elemento.", "success");
                        datatable.DataTable().ajax.reload(null, false);
                    }
                    else {
                        swal("Oops", "¡Ha ocurrido un error inesperado al intentar borrar el registro!", "error");
                    }
                },
                error: function (error) {
                    swal("Oops", "", "error");
                     var message = '¡Ocurrio un error inesperado al intentar eliminar los datos!';
                        if(error.responseJSON){
                            message = error.responseJSON.result;
                        }
                        swal("Error", message, "error");
                }
            });
        }
    });
}

function boolDataTableBadge(data, type, row, meta) {
    var estado = data.valueOf().toString().toLowerCase();
    if (estado.localeCompare("true") === 0){
        return '<span class="badge badge-success">Sí</span>';
    }
    else{
        return '<span class="badge badge-danger">No</span>';
    }
}

function decimalFormatDataTable(data, type, row, meta) {
    return parseFloat(data).toFixed(2);
}

var formAjaxSubmit = function(form, modal) {
    $(form).submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function (xhr, ajaxOptions, thrownError) {
                if ($(xhr).find('.is-invalid').length < 1) {
                    $defaultTable.DataTable().ajax.reload(null, false);
                    toastr.success('¡Se ha insertado el registro!', '¡Completado!')
                }

                $(modal).find('#form-modal-content').html(xhr);
                formAjaxSubmit(form, modal);
                loadModalDefaultComponents($(form));
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Oops", "¡Ocurrio un error al contactar al intentar contactar al servidor!", "error");
            }
        });
    });
};

var formAjaxUpdateSubmit = function(form, modal) {
    $(form).submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function (xhr, ajaxOptions, thrownError) {
                if ($(xhr).find('.is-invalid').length > 0) {
                    $(modal).find('#form-modal-content').html(xhr);
                    formAjaxUpdateSubmit(form, modal);
                    loadModalDefaultComponents($(form));
                }
                else {
                    $(modal).find('#form-modal-content').html('');
                    $(modal).modal('toggle');
                    $defaultTable.DataTable().ajax.reload(null, false);
                    toastr.warning('¡Se ha actualizado el registro!', '¡Completado!')
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Oops", "¡Ocurrio un error al contactar al intentar contactar al servidor!", "error");
            }
        });
    });
};

$('#search').on('keyup search input paste cut', function() {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$defaultTable.on('click', '.delete-button', function () {
    var url = $(this).data('url');
    var name = $(this).data('name');
    deleteDataTableItem(url, name, $defaultTable);
});

$defaultTable.on('click', '.edit-button', function () {
    var url = $(this).data('url');
    $('#form-modal-content').load(url, function () {
        $('#form-modal').modal('show');
        formAjaxUpdateSubmit('#form-modal-content form', '#form-modal');
    });
});

$('#add-button').click(function() {
    var url = $(this).data('url');
    $('#form-modal-content').load(url, function () {
        $('#form-modal').modal('show');
        formAjaxSubmit('#form-modal-content form', '#form-modal');
    });
});

$('#form-modal').on('shown.bs.modal', function () {
    loadModalDefaultComponents($(this));
});