from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not User.objects.filter(username='iqadmin').exists():
            User.objects.create_superuser('iqadmin', 'info@iqsd.co', '1Qerp.2017')
            self.stdout.write(self.style.SUCCESS('Successfully created new super user'))
